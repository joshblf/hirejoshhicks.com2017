# HireJoshHicks

The portfolio website for Josh Hicks.

### Prerequisites

```
node version 7+
```

### Installing

- Fork the main repository

- Clone the forked branch on to your local machine.

- Make sure you are using at least Node v7 or higher.  You could use something like [NVM](https://github.com/creationix/nvm) for that.

- Run `npm install` to get all of your node packages.

- Make sure you have Bower installed by running `bower -v`.  If you don't have it run `npm install -g bower`.  You may have to use `sudo npm install -g bower` if you get an error.

- create a file called `.bowerrc` in the root of the project and add this: 

    ```

    {
        "directory": "src/js/libs"
    }

    ```

- Run `bower install` to get your bower packages.

- Run `gulp` to compile everything and create your `/dist` folder.

- Open `/dist/index.html` to see the site in your browser.

## Authors

* **Josh Hicks** - *Initial work*
