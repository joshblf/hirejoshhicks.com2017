var gulp        = require('gulp'),
    browserSync = require('browser-sync'),
    watch       = require('gulp-watch'),
    cssmin		= require('gulp-minify-css'),
    concat		= require('gulp-concat'),
    uglify		= require('gulp-uglify')
    sass        = require('gulp-sass')
    imagemin    = require('gulp-imagemin')
    ;


//concat
gulp.task('concat-js', function(){
	gulp.src('app/js/*.js')
		.pipe(concat({path: 'all.js'}))
		.pipe(gulp.dest('app/dist'))
});

//uglify
gulp.task('uglify', function(){
	gulp.src('app/dist/all.js')
		.pipe(uglify())
		.pipe(gulp.dest('app/dist/min'))
});

//convert sass
gulp.task('sass', function () {
  gulp.src('app/sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('app/dist/css'));
});

//minify-css
gulp.task('cssmin', function(){
	gulp.src('app/dist/css/custom.css')
		.pipe(cssmin())
		.pipe(gulp.dest('app/dist/min'));
});

//Images
gulp.task('images', function () {
    return gulp.src('app/img/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
           
        }))
        .pipe(gulp.dest('app/dist/min/img'));
});


// Static server
gulp.task('watch', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch('index.html', [browserSync.reload]);
    gulp.watch('app/js/*.js', ['concat-js', 'uglify', browserSync.reload]);
    gulp.watch('app/sass/*.scss', ['sass', browserSync.reload]);
    gulp.watch('app/dist/css/*.css', ['cssmin', browserSync.reload]);
  
});

//defaul
gulp.task('default', ['images', 'concat-js', 'uglify', 'sass', 'cssmin', 'watch']);
